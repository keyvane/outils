/*p5.disableFriendlyErrors = true;

var pdf;

var hasard = 0;

let pg;
let textInput;
let tilesSlider;
let speedSlider;
let springSlider;
let ampXSlider;
let ampYSlider;
let textColorInput;
let bgColorInput;
let gradientStartInput = document.getElementById('gradientStartInput');
let gradientEndInput = document.getElementById('gradientEndInput');
let textF ;
let img = [];
let police = [];
let index = 0;
let indexf = 0;
let angle;
let opt;
let epaisseur;
let c =0;
let yscale = 0;





function preload() {

  for (var i = 0; i <6 ; i++) {
  img[i] = loadImage("img/0"+i+".png");
  }

  police[0]  = loadFont('font/Avara-Black.otf');
  police[1]  = loadFont('font/BluuNext-Bold.otf');
  police[2]  = loadFont('font/FengardoNeue_Black.otf');
  police[3]  = loadFont('font/FiraSans-Bold.ttf');
  police[4]  = loadFont('font/Format_1452.otf');
  police[5]  = loadFont('font/IBMPlexSans-Bold.ttf');
  police[6]  = loadFont('font/Millimetre-Extrablack.otf');
  police[7]  = loadFont('font/OfficeCodePro-Bold.ttf');
  police[8]  = loadFont('font/OstrichSans-Heavy.otf');
  police[9]  = loadFont('font/Roboto-Black.ttf');
  police[10]  = loadFont('font/Rubik-Black.ttf');
  police[11]  = loadFont('font/SpaceMono-Bold.ttf');

}

function setup() {
    frameRate(5);
    createCanvas(windowWidth, windowHeight);
    pdf = createPDF();
    pdf.beginRecord();
    createCanvas(windowWidth, windowHeight);
    pg = createGraphics(windowWidth, windowHeight);  
  
    textInput = document.getElementById('textInput');
    tilesSlider = document.getElementById('tilesSlider');
    speedSlider = document.getElementById('speedSlider');
    springSlider = document.getElementById('springSlider');
    ampXSlider = document.getElementById('ampXSlider');
    ampYSlider = document.getElementById('ampYSlider');
    textColorInput = document.getElementById('textColorInput');
    bgColorInput = document.getElementById('bgColorInput');
    textF = document.getElementById('textF');
    angle = document.getElementById('rotation');
    imageMode(CENTER);
    hasard = document.getElementById('hasard');
    opt = document.getElementById("mode");
    epaisseur = document.getElementById("epaisseur");
    rectMode(CENTER);
  
  }


  function draw() {



    textFont(police[11]);
    textSize(20);
    fill(255);
    stroke(255);
    background(0);
    translate(width/4, 200);
    for (var y = 0; y < height/2; y = y+ 20) {
    for (var x = 0; x < width/2 ; x = x+ 20) {
      
         
         push();
        
         cocos = cos(c + y) ;
         cmap = map(cocos, -1, 1, 0, 1 );
         translate(x, y);
         scale(1, cmap);         
         //text("A", 0, 0 - cocos *10);
         rect(0, 0, 18, 18);                 
         pop();    
        
        }
        //c += 0.06;

       }
       
       c += 0.6;
      
    }

    */


let rows, columns;

let xSpace, ySpace;

let yWave;
let yWaveSize = 20;
let yWaveSpeed = 0.01;

let ballSize = 10;

var pdf;

var hasard = 0;

let pg;
let textInput;
let tilesSlider;
let speedSlider;
let springSlider;
let ampXSlider;
let ampYSlider;
let textColorInput;
let bgColorInput;
let gradientStartInput = document.getElementById('gradientStartInput');
let gradientEndInput = document.getElementById('gradientEndInput');
let textF ;
let img = [];
let police = [];
let index = 0;
let indexf = 0;
let angle;
let opt;
let epaisseur;
let c =0;
let yscale = 0;

let mot = "HELLO WORLD"





function preload() {

  for (var i = 0; i <6 ; i++) {
  img[i] = loadImage("img/0"+i+".png");
  }

  police[0]  = loadFont('font/Avara-Black.otf');
  police[1]  = loadFont('font/BluuNext-Bold.otf');
  police[2]  = loadFont('font/FengardoNeue_Black.otf');
  police[3]  = loadFont('font/FiraSans-Bold.ttf');
  police[4]  = loadFont('font/Format_1452.otf');
  police[5]  = loadFont('font/IBMPlexSans-Bold.ttf');
  police[6]  = loadFont('font/Millimetre-Extrablack.otf');
  police[7]  = loadFont('font/OfficeCodePro-Bold.ttf');
  police[8]  = loadFont('font/OstrichSans-Heavy.otf');
  police[9]  = loadFont('font/Roboto-Black.ttf');
  police[10]  = loadFont('font/Rubik-Black.ttf');
  police[11]  = loadFont('font/SpaceMono-Bold.ttf');

}

function setup() {
  createCanvas(windowWidth, windowHeight);
   
  columnsSlider = createSlider(1,100,10);
  columnsSlider.position(30,30);
  columnsSlider.style('width','100px');
  
  rowsSlider = createSlider(1,100,10);
  rowsSlider.position(30,60);
  rowsSlider.style('width','100px');  
  
  xSpaceSlider = createSlider(0,200,20);
  xSpaceSlider.position(150,30);
  xSpaceSlider.style('width','100px');
  
  ySpaceSlider = createSlider(0,200,20);
  ySpaceSlider.position(150,60);
  ySpaceSlider.style('width','100px');
}

function draw() {
  background('#000000');
  
  textFont(police[11]);
  textSize(20);
  
  text("Columns",30,30);
  text("Rows",30,60);
  text("Column Spacing",150,30);    
  text("Row Spacing",150,60);   

  // Connect the slider values to the wave variables
  rows = rowsSlider.value();
  columns = columnsSlider.value();
  xSpace = xSpaceSlider.value();
  ySpace = ySpaceSlider.value();
  
  // Center matrix
  translate(width/2,height/2);
  
  // Reposition  matrix depending on width & height of the grid
  translate( -(columns-1) * xSpace/2, -(rows-1) * ySpace/2);
  
  noStroke();
  fill('#f9c80e');
  
  // grid
  for(var i = 0; i < columns; i++){
    for(var j = 0; j < rows; j++){
      push();
        // grid spacing 
       // translate(i*xSpace,j*ySpace);

       translate(i*50,j*50);
        // wave
       
        yWave = sin(frameCount * yWaveSpeed + i*0.5) * yWaveSize;
       var s = map(yWave, -20, 20, 0,1);
      scale(1, s);
      textSize(50);
      //  translate(0,yWave);
        //ellipse(0,0,10,10);
      text("A", 0,0);
      pop();
    }
  }
}