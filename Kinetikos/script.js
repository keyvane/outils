p5.disableFriendlyErrors = true;

var pdf;

var hasard = 0;

let pg;
let textInput;
let tilesSlider;
let speedSlider;
let springSlider;
let ampXSlider;
let ampYSlider;
let textColorInput;
let bgColorInput;
let gradientStartInput = document.getElementById('gradientStartInput');
let gradientEndInput = document.getElementById('gradientEndInput');
let textF ;
let img = [];
let police = [];
let index = 0;
let indexf = 0;
let angle;
let opt;
let epaisseur;



function preload() {

  for (var i = 0; i <6 ; i++) {
  img[i] = loadImage("img/0"+i+".png");
  }

  police[0]  = loadFont('font/Avara-Black.otf');
  police[1]  = loadFont('font/BluuNext-Bold.otf');
  police[2]  = loadFont('font/FengardoNeue_Black.otf');
  police[3]  = loadFont('font/FiraSans-Bold.ttf');
  police[4]  = loadFont('font/Format_1452.otf');
  police[5]  = loadFont('font/IBMPlexSans-Bold.ttf');
  police[6]  = loadFont('font/Millimetre-Extrablack.otf');
  police[7]  = loadFont('font/OfficeCodePro-Bold.ttf');
  police[8]  = loadFont('font/OstrichSans-Heavy.otf');
  police[9]  = loadFont('font/Roboto-Black.ttf');
  police[10]  = loadFont('font/Rubik-Black.ttf');
  police[11]  = loadFont('font/SpaceMono-Bold.ttf');

}


function setup() {
  frameRate(30);
  createCanvas(windowWidth, windowHeight, P2D);
  pdf = createPDF();
  pdf.beginRecord();
  createCanvas(windowWidth, windowHeight);
  pg = createGraphics(windowWidth, windowHeight);


  textInput = document.getElementById('textInput');
  tilesSlider = document.getElementById('tilesSlider');
  speedSlider = document.getElementById('speedSlider');
  springSlider = document.getElementById('springSlider');
  ampXSlider = document.getElementById('ampXSlider');
  ampYSlider = document.getElementById('ampYSlider');
  textColorInput = document.getElementById('textColorInput');
  bgColorInput = document.getElementById('bgColorInput');
  textF = document.getElementById('textF');
  angle = document.getElementById('rotation');
  imageMode(CENTER);
  hasard = document.getElementById('hasard');
  opt = document.getElementById("mode");
  epaisseur = document.getElementById("epaisseur");


}

function draw() {


 
  var choix = opt.options[opt.selectedIndex].value;
 
  const stringWidth = textWidth(textInput.value);
  const textSize = (width / stringWidth) * 10;

  background(bgColorInput.value);
  pg.background(bgColorInput.value);  
  pg.textSize(int(textF.value));
  pg.textFont(police[indexf]);
  pg.applyMatrix();
  pg.translate(width / 2, height / 2);

  if ($('#contours').is(":checked")) {
    pg.stroke(textColorInput.value);
    pg.strokeWeight(epaisseur.value);
    pg.noFill();
  } else {
    pg.fill(textColorInput.value);
    pg.noStroke();
  }

  if (choix == "rectangles") {
    for (var x = 0; x < width/2; x += 20) {
      for (var  y = 0; y < width/2; y += 20) {         
         pg.push();
         pg.translate(x-150, y-150);
         pg.rotate(int(radians(angle.value)));
         pg.rect(0, 0, textF.value/20, textF.value/20);
         pg.pop();
      }
    }
  }

  
  if (choix == "ellipses") {
    for (var x = 0; x < width/2; x += 20) {
      for (var  y = 0; y < width/2; y += 20) {
         
         pg.push();
         pg.translate(x-150, y-150);
         pg.rotate(int(radians(angle.value)));
         //pg.stroke(255);
         pg.ellipse(0, 0, textF.value /20, textF.value/20);
         pg.pop();
      }
    }
  }


  if (choix == "lines") {
    for (var x = 0; x < width/2; x += 20) {
      for (var  y = 0; y < width/2; y += 20) {
         
         pg.push();
         pg.translate(x-150, y-150);
         pg.rotate(int(radians(angle.value)));
         //pg.stroke(255);
         pg.line(0,0 , textF.value/20, textF.value/20); 
         pg.pop();
      }
    }
  }

  if (choix == "triangles") {
    for (var x = 0; x < width/2; x += 20) {
      for (var  y = 0; y < width/2; y += 20) {
         
         pg.push();
         pg.translate(x-150, y-150);
         pg.rotate(int(radians(angle.value)));
         //pg.stroke(255);
         pg.triangle(0,0 , 0, textF.value/20 ,  textF.value/20,(textF.value/20)/2); 
         pg.pop();
      }
    }
  }

  if (choix == "text") {
    pg.rotate(int(radians(angle.value)));
    pg.textAlign(CENTER, CENTER);
    pg.text(textInput.value, 0, 0);
    
  }

  if (choix == "images") {
    pg.rotate(int(radians(angle.value)));
    pg.image(img[index], 0, 0, textF.value,textF.value);
    
  }


  pg.resetMatrix();

  const tilesX = tilesSlider.value;
  const tilesY = tilesX;

  let tileW = width / tilesX;
  let tileH = height / tilesY;

  for (let y = 0; y < tilesY; y++) {
    for (let x = 0; x < tilesX; x++) {
      // WARP
      let wave;
      // wave = int(sin(frameCount * 0.05 + ( x * y ) * 0.07) * 100)
      // const speedOrSeed = frameCount / 10 // dynamic
      const speedOrSeed = frameCount * (speedSlider.value * .1); // dynamic
      const amplitude = 50; // 1+
      const offset = x * y;
      // const spring = 0.07 // 0 -
      const spring = springSlider.value * 0.01;
      wave = sin(speedOrSeed + offset * spring);

      const sourceX = wave * ampXSlider.value;
      const sourceY = wave * ampYSlider.value;

   
      if ($('#hasard').is(':checked') == true) {
      // SOURCE
      const sx = x * tileW + sourceX;
      const sy = y * tileH + sourceY;
      const sw = tileW + random(0,tileW);
      const sh = tileH + random(0,tileH);

      // DEST
      const dx = x * tileW;
      const dy = y * tileH;
      const dw = tileW + random(0,tileW);
      const dh = tileH + random(0,tileH);

      copy(pg, sx, sy, sw, sh, dx, dy, dw, dh);
      } else {
    


         // SOURCE
      const sx = x * tileW + sourceX;
      const sy = y * tileH + sourceY;
      const sw = tileW ;
      const sh = tileH ;

      // DEST
      const dx = x * tileW;
      const dy = y * tileH;
      const dw = tileW ;
      const dh = tileH ;
      copy(pg, sx, sy, sw, sh, dx, dy, dw, dh);
      }
      

    }
  }



}

function keyPressed() {
  if (keyCode === ENTER) {
  noLoop();
  pdf.save();
  }

}

function change_img() {
    if (index > 4) {
      index = 0;
    } else {
    index ++;
    }

}

function change_font() {
    if (indexf > 9) {
      indexf = 0;
    } else {
    indexf ++;
    }
}


